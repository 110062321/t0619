const {ccclass, property} = cc._decorator;
@ccclass
export default class ai1 extends cc.Component {
    @property(cc.Node)
    enemy: cc.Node = null;
    @property(cc.AudioClip)
    winSE: cc.AudioClip = null;
    @property(cc.AudioClip)
    loseSE: cc.AudioClip = null;
    
    private isMovingLeft: boolean = false;
    private isMovingRight: boolean = false;
    private isMovingUp: boolean = false;
    private isMovingDown: boolean = false;
    private isGood: boolean = false;
    randNum:number;
    pMoveSpeed: number;
    eMoveSpeed: number;
    begin:number;

    start(){
        this.pMoveSpeed=200;
        this.eMoveSpeed=100;
        this.randNum = Math.floor(Math.random() *441)+50;
        this.enemy.y=this.randNum;
        this.begin=0;
        this.scheduleOnce(this.countdown1,3);
        this.scheduleOnce(this.countdown2,18);
    }
    countdown1(){
        this.begin++;
    }
    countdown2(){
        this.begin++;
    }
    onLoad(){
        cc.director.getPhysicsManager().enabled=true;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }
    update(dt){
        if(this.begin==2){
            cc.audioEngine.stopMusic();
            cc.audioEngine.playEffect(this.winSE, false);
            cc.director.loadScene("default_end");
        }
        this.playerMovement(dt);
        this.enemyMovement(dt);
    }
    playerMovement(dt){
        if (this.isMovingLeft) {
            this.node.x -= this.pMoveSpeed * dt;
        } 
        if (this.isMovingRight) {
            this.node.x += this.pMoveSpeed * dt;
        }
        if (this.isMovingUp) {
            this.node.y += this.pMoveSpeed * dt;
        } 
        if (this.isMovingDown) {
            this.node.y -= this.pMoveSpeed * dt;
        }
    }
    enemyMovement(dt) {
        if(this.begin){
            const direction = this.node.position.sub(this.enemy.position);
            const distance = direction.mag();
            const normalizedDirection = direction.normalize();
            const speed = this.eMoveSpeed * dt;
            const movement = normalizedDirection.mul(speed);
            if (distance > speed) {
            this.enemy.position = this.enemy.position.add(movement);
            } else {
            this.enemy.position = this.node.position.clone();
            }
        }
    }
    onKeyDown(e){
        if (e.keyCode === cc.macro.KEY.j) {
            this.isMovingLeft = true;
        } 
        if (e.keyCode === cc.macro.KEY.l) {
            this.isMovingRight = true;
        }
        if (e.keyCode === cc.macro.KEY.i) {
            this.isMovingUp = true;
        } 
        if (e.keyCode === cc.macro.KEY.k) {
            this.isMovingDown = true;
        }
        if (e.keyCode === cc.macro.KEY.space) {
            this.isGood = true;
        }
    }
    onKeyUp(e){
        if (e.keyCode === cc.macro.KEY.j) {
            this.isMovingLeft = false;
        } 
        if (e.keyCode === cc.macro.KEY.l) {
            this.isMovingRight = false;
        }
        if (e.keyCode === cc.macro.KEY.i) {
            this.isMovingUp = false;
        } 
        if (e.keyCode === cc.macro.KEY.k) {
            this.isMovingDown = false;
        }
    }
    onBeginContact(contact,self,other){
        if((other.node.name=="enemy1"||other.node.name=="enemy2")&&!this.isGood){
            cc.audioEngine.stopMusic()
            cc.audioEngine.playEffect(this.loseSE,false);
            cc.director.loadScene("default_end");
        } 
    }
}
